package com.accolades.githubmicroservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accolades.githubmicroservice.model.GithubUserRepoMaster;

public interface GithubUserRepoMasterRepository extends JpaRepository<GithubUserRepoMaster,Long>{

}
