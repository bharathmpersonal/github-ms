package com.accolades.githubmicroservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accolades.githubmicroservice.model.GithubUserMaster;

public interface GithubUserMasterRepository extends JpaRepository<GithubUserMaster,Long>{

}
