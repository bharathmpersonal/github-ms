package com.accolades.githubmicroservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accolades.githubmicroservice.model.User;

public interface UserRepository extends JpaRepository<User,Long>{

}
