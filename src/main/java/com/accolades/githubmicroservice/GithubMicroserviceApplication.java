package com.accolades.githubmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class GithubMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GithubMicroserviceApplication.class, args);
	}

}
