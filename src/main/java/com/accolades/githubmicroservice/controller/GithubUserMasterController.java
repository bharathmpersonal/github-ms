package com.accolades.githubmicroservice.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.accolades.githubmicroservice.model.GithubUserMaster;
import com.accolades.githubmicroservice.model.GithubUserRepoMaster;
import com.accolades.githubmicroservice.model.User;
import com.accolades.githubmicroservice.repository.GithubUserMasterRepository;
import com.accolades.githubmicroservice.repository.GithubUserRepoMasterRepository;
import com.accolades.githubmicroservice.repository.UserRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class GithubUserMasterController {

	 Logger logger = LoggerFactory.getLogger(GithubUserMasterController.class);
	 @Autowired
	 private GithubUserMasterRepository githubUserMasterRepository;
	 
	 @Autowired
	 private GithubUserRepoMasterRepository githubUserRepoMasterRepository;
	
	 @Autowired
	 private UserRepository userRepository;
	 
	@GetMapping("/create")
    public String createQuestion() throws IOException {
//        return githubUserMasterRepository.save(githubUserMaster);
		int lastloggedInUser = 0;
		RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.setBasicAuth("accoladesindia", "tech*3692");
			HttpEntity<String> entity = new HttpEntity<String>(headers);
			int sinceValue = 0;

			for (int i = 0; i < 20; i++) {
				//get 10 users
				ResponseEntity<String> response = restTemplate.exchange(
						"https://api.github.com/users?since=" + sinceValue + "&per_page=100", HttpMethod.GET, entity,
						String.class);
			
				ObjectMapper mapper = new ObjectMapper();
				List<GithubUserMaster> list = mapper.readValue(response.getBody(),new TypeReference<List<GithubUserMaster>>() {});
				
				//iterate each user
				for(GithubUserMaster githubUserMaster : list) {
					logger.info("user id is "+githubUserMaster.getId()+" and user name is "+githubUserMaster.getLogin());
					//get each user location
					ResponseEntity<GithubUserMaster> responsegithubUserMaster = restTemplate.exchange(
							githubUserMaster.getUrl(), HttpMethod.GET, entity,
							GithubUserMaster.class);
					GithubUserMaster userMasterDetail  = githubUserMasterRepository.save(responsegithubUserMaster.getBody());
					User user = new User();
					user.setFollowers(userMasterDetail.getFollowers());
					user.setLoggedInUserId(userMasterDetail.getId());
					user.setLoggedInUserName(userMasterDetail.getLogin());
					user.setPublicRepos(userMasterDetail.getPublic_repos());
					user.setUserLocation(userMasterDetail.getLocation());
					userRepository.save(user);
					
					ResponseEntity<String> response123 = restTemplate.exchange(
							githubUserMaster.getRepos_url(), HttpMethod.GET, entity,
							String.class);
					
					//get all repos of that users
					ObjectMapper mapper123 = new ObjectMapper();
					List<GithubUserRepoMaster> listnew = mapper123.readValue(response123.getBody(),new TypeReference<List<GithubUserRepoMaster>>() {});
					for(GithubUserRepoMaster githubUserRepoMaster : listnew) {
						logger.info("user id is "+githubUserMaster.getId()+" and user name is "+githubUserMaster.getLogin()+" and repo id is "+githubUserRepoMaster.getId());
						
						githubUserRepoMaster.setOwner_id(githubUserRepoMaster.getOwner().getId());
						githubUserRepoMasterRepository.save(githubUserRepoMaster);
						
					}

				}
				sinceValue = list.get(list.size()-1).getId();
			}
			
			
			
			
        return null;
    }
}
