package com.accolades.githubmicroservice.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "app_user")
public class User extends AuditModel {
    @Id
    @GeneratedValue(generator = "user_master_generator")
    @SequenceGenerator(
            name = "user_master_generator",
            sequenceName = "user_master_generator_sequence",
            initialValue = 1
    )
    private Long id;
	private String loggedInUserName;
	private int loggedInUserId;
	private String userLocation;
	private int publicRepos;
	private int followers;
	private double totalScore;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLoggedInUserName() {
		return loggedInUserName;
	}
	public void setLoggedInUserName(String loggedInUserName) {
		this.loggedInUserName = loggedInUserName;
	}
	
	public String getUserLocation() {
		return userLocation;
	}
	public void setUserLocation(String userLocation) {
		this.userLocation = userLocation;
	}
	public int getPublicRepos() {
		return publicRepos;
	}
	public void setPublicRepos(int publicRepos) {
		this.publicRepos = publicRepos;
	}
	public int getFollowers() {
		return followers;
	}
	public void setFollowers(int followers) {
		this.followers = followers;
	}
	
	public int getLoggedInUserId() {
		return loggedInUserId;
	}
	public void setLoggedInUserId(int loggedInUserId) {
		this.loggedInUserId = loggedInUserId;
	}
	public double getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(double totalScore) {
		this.totalScore = totalScore;
	}
	
}
