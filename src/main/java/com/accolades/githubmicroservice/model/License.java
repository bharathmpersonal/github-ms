package com.accolades.githubmicroservice.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "license")
public class License {

	@Id
    @GeneratedValue(generator = "github_license_generator")
    @SequenceGenerator(
            name = "github_license_generator",
            sequenceName = "github_license_generator_sequence",
            initialValue = 1
    )
    private Long license_id;
	private String key;
	private String name;
	private String spdx_id;
	private String url;
	private String node_id;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSpdx_id() {
		return spdx_id;
	}
	public void setSpdx_id(String spdx_id) {
		this.spdx_id = spdx_id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getNode_id() {
		return node_id;
	}
	public void setNode_id(String node_id) {
		this.node_id = node_id;
	}
	public Long getLicense_id() {
		return license_id;
	}
	public void setLicense_id(Long license_id) {
		this.license_id = license_id;
	}
	
}
